# Ansible Operator - Chronicler
An Ansible-based Operator, for the corresponding [Chronicler Role].

Minimal files to form a functional operator. In this early version
we just introduce the conventional `build` and `deploy` directories.
There should be much more for testing but, for now, this is all we need.

Our operator logic is encoded in a separate Ansible Role that's
installed from [Ansible Galaxy] during the build. This way we keep our
application logic and operator implementation separate.
 
The documentation for the [Ansible SDK] has plenty of information on
building and deploying operators, what follows is a summary of what you can
find there.  

## Deploying (Kubernetes)

Deploy the CRD: -

    $ kubectl create -f deploy/crds/chronicler_v1_chronicler_crd.yaml

Create and move to a new namespace: -

    $ kubectl create -f deploy/namespace.yaml
    $ kubectl config set-context --current --namespace=chronicler
    
Deploy the operator: -

    $ kubectl create -f deploy/service_account.yaml
    $ kubectl create -f deploy/role.yaml
    $ kubectl create -f deploy/role_binding.yaml
    $ kubectl create -f deploy/operator.yaml

Deploy the app: -

    $ kubectl apply -f deploy/crds/chronicler_v1_chronicler_cr_1.yaml

Un-deploy (which simply sets the role's `state` variable to `absent`): -

    $ kubectl apply -f deploy/crds/chronicler_v1_chronicler_cr_absent.yaml

---

[ansible galaxy]: https://galaxy.ansible.com/matildapeak/chronicler
[ansible sdk]: https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md
[chronicler role]: https://github.com/MatildaPeak/ansible-role-chronicler
